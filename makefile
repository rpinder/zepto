zepto: zepto.c
	$(CC) zepto.c -o zepto -Wall -Wextra -pedantic -std=c99

install: zepto
	cp $< /usr/local/bin/zepto
